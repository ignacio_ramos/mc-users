package com.ramosvji.mcusers;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@EnableEurekaClient
@SpringBootApplication
public class McUsersApplication {

	public static void main(String[] args) {
		SpringApplication.run(McUsersApplication.class, args);
	}

}
