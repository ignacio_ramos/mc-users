package com.ramosvji.mcusers.controller.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ramosvji.mcusers.controller.UsersController;
import com.ramosvji.mcusers.entity.User;
import com.ramosvji.mcusers.service.UsersService;

@RestController
@RequestMapping(path="/ramosvji/api")
public class UsersControllerImpl implements UsersController {
	
	@Autowired
	UsersService usersService;

	@GetMapping(path="/v01/user/{username}")
	@Override
	public ResponseEntity<User> getUserByUsername(final @PathVariable("username") String username) {
		User user = usersService.findByUsername(username);
		
		return new ResponseEntity<User>(user, new HttpHeaders(), HttpStatus.OK);
	} 

}
