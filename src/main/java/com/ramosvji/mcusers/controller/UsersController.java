package com.ramosvji.mcusers.controller;

import org.springframework.http.ResponseEntity;

import com.ramosvji.mcusers.entity.User;

public interface UsersController {
	
	//public ResponseEntity<List<User>> getUsers();
	public ResponseEntity<User> getUserByUsername(String username);

}
