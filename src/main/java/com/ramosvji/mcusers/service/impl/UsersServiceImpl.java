package com.ramosvji.mcusers.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ramosvji.mcusers.entity.User;
import com.ramosvji.mcusers.repository.UsersRepository;
import com.ramosvji.mcusers.service.UsersService;

@Service
public class UsersServiceImpl implements UsersService {
	
	@Autowired
	private UsersRepository usersRepository; 


	@Override
	public List<User> getAllUsers() {
		return usersRepository.findAll();
	}

	@Override
	public User findByUsername(String username) {
		return usersRepository.findByUsername(username);
	}

}
