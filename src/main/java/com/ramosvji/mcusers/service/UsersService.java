package com.ramosvji.mcusers.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.ramosvji.mcusers.entity.User;

@Service
public interface UsersService {
	public List<User> getAllUsers();
	public User findByUsername(String username);
}
