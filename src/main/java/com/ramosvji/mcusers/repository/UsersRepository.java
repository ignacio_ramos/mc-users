package com.ramosvji.mcusers.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.ramosvji.mcusers.entity.User;

@Repository
public interface UsersRepository extends MongoRepository<User,String>{
	public User findByUsername(String username);
}
